from django.shortcuts import render
from django.views.generic import ListView, TemplateView, DetailView
from .models import Post

class HomePageView(TemplateView):
    template_name = "home.html"

class BlogListView(ListView):
    model = Post
    template_name = "blog.html"

class BlogDetailView(DetailView):
    model = Post
    template_name = "post_detail.html"

class CVPageView(TemplateView):
    template_name = "cv.html"
