from django.urls import path

from .views import BlogListView, HomePageView, BlogDetailView, CVPageView

urlpatterns = [
    path("", HomePageView.as_view(), name="home"),
    path("blog/", BlogListView.as_view(), name="blog"),
    path("post/<int:pk>/", BlogDetailView.as_view(), name="post_detail"),
    path("cv/", CVPageView.as_view(), name="cv")
]
